# Driver script to write multiple GDML files:
#     1. OLYMPUS_RootNoTor.gdml  --- for the EventDisplay with the toroid removed
#     2. OLYMPUS_Fitting.gdml    --- for use with the track fitting plugins
#     2. OLYMPUS_Root.gdml       --- for the EventDisplay
#     3. OLYMPUS_Geant4.gdml     --- for the Monte Carlo
#
# The files generateGDML.py and olympusWriter.py should be stored in the same folder as this script.
# To import the module olympusWriter, you must add $ROOTSYS/geom/gdml to your PYTHONPATH environment variable.  Verify that the file writer.py is in that folder.
# This expects a set of text files describing component geometries to be stored in the same directory.
#
#
#
# VERSION 1.2
# May 24, 2012
# C. O'Connor
#
# Written on a Mac running Python 2.6.1 and GCC 4.2.1 on Darwin
###########################################################################################################################################################

# import sys
from gdmlModule import *
import os

class GDML_Generator:
    def __init__(self,tag):
        self.tag = tag
        self.matfile = os.path.dirname(os.path.realpath(__file__))+'/materials.txt'
        self.worldname = 'world_log'
        self.omit = []
        self.solData = None
        self.volData = None
        self.posData = None
        self.rotData = None
        self.outDir = None
    def SetSolids(self,solData):
        self.solData = solData
    def SetVolumes(self,volData):
        self.volData = volData
    def SetPositions(self,posData):
        self.posData = posData
    def SetRotations(self,rotData):
        self.rotData = rotData
    def SetOutDir(self,outDir):
        self.outDir = outDir
    def MakeGDML(self):
        assert self.solData is not None
        assert self.volData is not None
        assert self.posData is not None
        assert self.rotData is not None
        assert self.outDir is not None
        print "\n\nWelcome!  Let's generate some beautiful GDML files together.\n\n"
        gdmlfileRoot = self.outDir+'/Root_' + self.tag + '.gdml'
        gdmlRoot = olympusWriter.olympusWriter(gdmlfileRoot)
        gdmlRoot.worldname = self.worldname

        print '\nGenerating ' + gdmlRoot.gdmlfile
        print 'World volume: ' + gdmlRoot.worldname

        print '\nBuilding positions and rotations...'
        buildPositionsRotations(gdmlRoot, self.posData, self.rotData)

        print '\nBuilding materials...'
        print 'Using materials as defined in ' + self.matfile
        nongaseous = []
        buildMaterials(gdmlRoot, self.matfile, nongaseous)

        print '\nBuilding solids...'
        # print 'Using solids as defined in ' + solfile
        gdmlRoot.ignoreNoSolid()
        buildSolids(gdmlRoot, self.solData)


        print '\nNow back to working on ' + gdmlRoot.gdmlfile

        print '\nBuilding volumes...'
        # print 'Using volumes as defined in ' + volfile
        gdmlRoot.ignoreNoVolume()
        gdmlRoot.ignoreVolumes(self.omit)
        buildVolumes(gdmlRoot, self.volData, False)

        # copy the gdmlRoot olympusWriter and build the gdmlGeant4 olympusWriter

        gdmlfileGeant4 = self.outDir+'/Geant4_' + self.tag + '.gdml'
        print '\nCopying ' + gdmlRoot.gdmlfile + ' content to ' + gdmlfileGeant4
        gdmlGeant4 = gdmlRoot.copy()
        gdmlGeant4.worldname = self.worldname
        gdmlGeant4.setFile(gdmlfileGeant4)

        print '\nGenerating ' + gdmlGeant4.gdmlfile + ' ...'
        print 'World volume: ' + gdmlGeant4.worldname


        # print '\nBuilding the collimator volumes...'
        # # print 'Using volumes as defined in ' + volfile
        # # gdmlGeant4.removeVolume('TC_log0')
        # # gdmlGeant4.removeVolume('WS_log0')
        # gdmlGeant4.ignoreNoVolume()
        # # gdmlGeant4.ignoreVolumes(omit)
        # buildVolumes(gdmlGeant4, volfile, False)


        # for each GDML file:
        #     add the World volume, incorporating whichever volumes are appropriate
        #     call addSetup(name, version, world)
        #     write the file

        olympusWriters = {gdmlRoot: self.volData,
                          gdmlGeant4: self.volData
                          }
        sub = 0

        for owriter in olympusWriters:
            print '\nBuilding the World volume for ' + owriter.gdmlfile
            buildVolumes(owriter, olympusWriters[owriter])
            owriter.addSetup('Default', '3.' + str(sub), owriter.worldname)
            print 'Writing ' + owriter.gdmlfile
            owriter.writeFile()
            sub += 1

        print "\nLooks like we made it... after all!"
