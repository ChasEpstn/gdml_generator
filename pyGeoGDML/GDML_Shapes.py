from __future__ import division
from math import *
from GDML_Util import *
import os
from generateGDML import *
import cStringIO

def SETEXPERIMENT(exp):
	Object.experiment = exp

class Detector:
	def __init__(self,prefix,objs=[]):
		self.prefix = prefix+"_"
		self.objs = objs
		self.world = Box("world",4000,4000,4000)
		# self.world.SetMaterial("G4_AIR")
		self.world.SetMaterial("G4_Galactic")
		self.world.SetColor("f999999999")
		self.solids = cStringIO.StringIO()#open(self.prefix+"solids.txt",'w')
		self.volumes = cStringIO.StringIO()#open(self.prefix+"volumes.txt",'w')
		self.positions = cStringIO.StringIO()#open(self.prefix+"positions.txt",'w')
		self.rotations = cStringIO.StringIO()#open(self.prefix+"rotations.txt",'w')
		self.positions.write("Position \t X \t Y \t Z \t Units\n\n")
		self.positions.write("zeropos \t 0 \t 0 \t 0 \t mm\n")
		self.rotations.write("Rotation \t X \t Y \t Z \t Units\n\n")
		self.rotations.write("zerorot \t 0 \t 0 \t 0 \t deg\n")
	def append(self,objt):
		self.objs.append(objt)
	# def MakeFiles(self):
	# 	self.solids = []#open(self.prefix+"solids.txt",'w')
	# 	self.volumes = []#open(self.prefix+"volumes.txt",'w')
	# 	self.positions = []#open(self.prefix+"positions.txt",'w')
	# 	self.rotations = []#open(self.prefix+"rotations.txt",'w')
	# 	self.positions.append("Position \t X \t Y \t Z \t Units\n\n")
	# 	self.positions.append("zeropos \t 0 \t 0 \t 0 \t mm\n")
	# 	self.rotations.append("Rotation \t X \t Y \t Z \t Units\n\n")
	# 	self.rotations.append("zerorot \t 0 \t 0 \t 0 \t deg\n")
	# def CloseFiles(self):
	# 	self.solids.close()
	# 	self.volumes.close()
	# 	self.positions.close()
	# 	self.rotations.close()
	def WriteSolids(self):
		self.solids.write("\n"+self.world.PrintConstruct()+"\n\n")
		for objt in self.objs:
			self.solids.write(objt.PrintConstruct()+"\n\n")
	def WritePositions(self):
		for objt in self.objs:
			if objt.IsVirtual() is False:
				self.positions.write(objt.PrintPosition()+"\n")
	def WriteRotations(self):
		for objt in self.objs:
			if objt.IsVirtual() is False:
				self.rotations.write(objt.PrintRotation()+"\n")
	def WriteVolumes(self):
		self.volumes.write("\n")
		for objt in self.objs:
			if objt.IsVirtual() is False:
				self.volumes.write(objt.PrintVolume()+"\n\n")
		self.volumes.write(self.world.PrintVolume()+"\n")
		for objt in self.objs:
			if objt.IsVirtual() is False:
				self.volumes.write(objt.PrintDaughter()+"\n")
	def Save(self):
		# self.MakeFiles()
		self.WriteSolids()
		self.WritePositions()
		self.WriteRotations()
		self.WriteVolumes()
		# self.CloseFiles()
	def SaveAndGenerate(self):
		self.Save()
		self.gen = GDML_Generator(self.prefix.strip('_'))
		self.solids.seek(0)
		self.volumes.seek(0)
		self.positions.seek(0)
		self.rotations.seek(0)
		self.gen.SetSolids(self.solids)
		self.gen.SetVolumes(self.volumes)
		self.gen.SetPositions(self.positions)
		self.gen.SetRotations(self.rotations)
		self.gen.SetOutDir(os.getcwd())
		self.gen.MakeGDML()

class Object:
	def GetKind(self):
		return self.kind
	def LogicalName(self):
		return self.name+"_log"
	def Name(self):
		return self.name
	def SetName(self,name):
		self.name = name
	def SetVirtual(self):
		self.virtual = True
	def UnsetVirtual(self):
		self.virtual = False
	def IsVirtual(self):
		try:
			return self.virtual #if has been set to be a placeholder
		except(AttributeError):
			return False #a real volume to be placed
	def SetPosition(self,pos):
		self.XPOS = pos[0]
		self.YPOS = pos[1]
		self.ZPOS = pos[2]
	def SetRotation(self,rot):
		self.XROT = rot[0]
		self.YROT = rot[1]
		self.ZROT = rot[2]
	def GetPosition(self):
		try:
			return [self.XPOS,self.YPOS,self.ZPOS]
		except(AttributeError):
			return [0,0,0]
	def GetRotation(self):
		try:
			return [self.XROT,self.YROT,self.ZROT]
		except(AttributeError):
			return [0,0,0]
	def PrintPosition(self):
		try:
			return self.Name()+"_pos\t %g \t %g \t %g \t mm" %(self.XPOS,self.YPOS,self.ZPOS)
		except(AttributeError):
			return self.Name()+"_pos\t %g \t %g \t %g \t mm" %(0,0,0)
	def PrintRotation(self):
		try:
			return self.Name()+"_rot\t %g \t %g \t %g \t deg" %(self.XROT,self.YROT,self.ZROT)
		except(AttributeError):
			return self.Name()+"_rot\t %g \t %g \t %g \t deg" %(0,0,0)
	def SetMaterial(self,mat):
		self.material = mat
	def SetColor(self,color):
		self.color = color
	def Color(self):
		try:
			return self.color
		except(AttributeError):
			return "t663675714"
	def SetSensitive(self,name):
		self.sensname = name
	def PrintVolume(self):
		try:
			return "Logical Volume: "+self.LogicalName()+"\n\t Solid: "+self.Name()+"\n\t Material: "+self.material+"\n\t Color: "+self.Color()+"\n\t Sensitive Detector: "+ self.sensname
		except(AttributeError):
			return "Logical Volume: "+self.LogicalName()+"\n\t Solid: "+self.Name()+"\n\t Material: "+self.material+"\n\t Color: "+self.Color()
	def PrintDaughter(self):
		return "\t\t Daughter (Physical Volume): "+self.Name()+"\n\t\t\t Daughter Logical Volume: "+self.LogicalName()+"\n\t\t\t Daughter Position: "+self.Name()+"_pos \n\t\t\t Daughter Rotation: "+self.Name()+"_rot"
	def Fill(self):
		pass
	def FillVacuum(self):
		VAC = self.Fill()
		VAC.SetPosition(self.GetPosition())
		VAC.SetRotation(self.GetRotation())
		VAC.SetMaterial("G4_Galactic")
		self.experiment.append(VAC)
		return VAC

class Box(Object):
	def __init__(self,name,dx,dy,dz):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.dx = dx
		self.dy = dy
		self.dz = dz
	def PrintConstruct(self):
		return "G4Box: "+self.Name()+"\n\t dx = %g [mm] \n \t dy = %g [mm] \n \t dz = %g [mm]" %(self.dx,self.dy,self.dz)
	def x(self):
		return self.dx
	def y(self):
		return self.dy
	def z(self):
		return self.dz

class Tube(Object):
	def __init__(self,name,rmin,rmax,z,startphi,deltaphi):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.rmin = rmin
		self.rmax = rmax
		self.z = z
		self.startphi = startphi
		self.deltaphi = deltaphi
	def PrintConstruct(self):
		return "G4Tube: "+self.Name()+"\n\t rmin = %g [mm] \n \t rmax = %g [mm] \n \t z = %g [mm] \n \t startphi = %g [deg] \n \t deltaphi = %g [deg]" %(self.rmin,self.rmax,self.z,self.startphi,self.deltaphi)
	def Fill(self):
		return Tube(self.name+"_Fill",0,self.rmin,self.z,self.startphi,self.deltaphi)

class SubtractionSolid(Object):
	def __init__(self,name,solidA,solidB,pos=[0,0,0],rot=[0,0,0]):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.SetPosition(solidA.GetPosition())
		self.SetRotation(solidA.GetRotation())
		self.name = name
		self.solidA = solidA.Name()
		self.solidB = solidB.Name()
		self.VacSolid = solidB
		self.xp = pos[0]
		self.yp = pos[1]
		self.zp = pos[2]
		self.xr = rot[0]
		self.yr = rot[1]
		self.zr = rot[2]
	def FillVacuum(self):
		VAC = CopyObject(self.VacSolid)
		VAC.SetName(self.name+"_vacs")
		VAC.SetPosition([i+j for i,j in zip(self.GetPosition(),[self.xp,self.yp,self.zp])])
		VAC.SetRotation([i+j for i,j in zip(self.GetRotation(),[self.xr,self.yr,self.zr])])
		VAC.SetMaterial("G4_Galactic")
		self.experiment.append(VAC)
		return VAC
	def PrintConstruct(self):
		return "G4SubtractionSolid: "+self.Name()+"\n\t Solid A: "+self.solidA+"\n\t Solid B: "+self.solidB+"\n \t \t Position of B relative to A: "+self.solidA+"_"+self.solidB+"_pos"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [mm] \n \t \t Rotation of B relative to A: "%(self.xp,self.yp,self.zp)+self.solidA+"_"+self.solidB+"_rot"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [deg]" %(self.xr,self.yr,self.zr)

class UnionSolid(SubtractionSolid):
	def PrintConstruct(self):
		return "G4UnionSolid: "+self.Name()+"\n\t Solid A: "+self.solidA+"\n\t Solid B: "+self.solidB+"\n \t \t Position of B relative to A: "+self.solidA+"_"+self.solidB+"_pos"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [mm] \n \t \t Rotation of B relative to A: "%(self.xp,self.yp,self.zp)+self.solidA+"_"+self.solidB+"_rot"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [deg]" %(self.xr,self.yr,self.zr)

class IntersectionSolid(SubtractionSolid):
	def PrintConstruct(self):
		return "G4IntersectionSolid: "+self.Name()+"\n\t Solid A: "+self.solidA+"\n\t Solid B: "+self.solidB+"\n \t \t Position of B relative to A: "+self.solidA+"_"+self.solidB+"_pos"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [mm] \n \t \t Rotation of B relative to A: "%(self.xp,self.yp,self.zp)+self.solidA+"_"+self.solidB+"_rot"+"\n\t\t\t (x, y, z) = (%g, %g, %g) [deg]" %(self.xr,self.yr,self.zr)

class Trapezoid(Object):
	def __init__(self,name,dx1,dx2,dy1,dy2,dz):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.dx1 = dx1
		self.dx2 = dx2
		self.dy1 = dy1
		self.dy2 = dy2
		self.dz = dz
	def PrintConstruct(self):
		return "G4Trd: "+self.Name()+"\n\t dx1 = %g [mm] \n\t dx2 = %g [mm] \n\t dy1 = %g [mm] \n\t dy2 = %g [mm] \n\t dz = %g [mm]" %(self.dx1,self.dx2,self.dy1,self.dy2,self.dz)

class Wedge(Object):#z, theta, phi, y1, x1, x2, alpha1, y2, x3, x4, alpha2
	def __init__(self,name,base,height,thk,angle):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.base = base
		self.height = height
		self.thk = thk
		self.angle = angle
		self.ext = self.height/tan(pi-degToRad(self.angle))
		print 'self.ext',self.ext
		# self.alpha = radToDeg(atan(self.height/(self.base+self.ext)))
		# self.beta = 180-self.angle-self.alpha
		# self.gamma = 90 - (self.alpha + self.beta/2.0)
		self.gamma = radToDeg(atan((self.base/2.0+self.ext)/self.height))
		self.XROT = -90
		self.YROT = 0
		self.ZROT = 90
		self.XPOS = 0
		self.YPOS = self.height/2.0
		self.ZPOS = 3.0*self.base/4.0 + self.ext/2.0
	def SetPosition(self,pos):
		self.XPOS += pos[0]
		self.YPOS += pos[1]
		self.ZPOS += pos[2]
	def SetRotation(self,rot):
		self.XROT += rot[0]
		self.YROT += rot[1]
		self.ZROT += rot[2]
	def PrintConstruct(self):
		return "G4Trap: "+self.Name()+"\n\t pDz = %g [mm] \n\t pTheta = %g [mm] \n\t pPhi = %g [mm] \n\t pDy1 = %g [mm] \n\t pDx1 = %g [mm] \n\t pDx2 = %g [mm]  \n\t pAlp1 = %g [mm]  \n\t pDy2 = %g [mm]  \n\t pDx3 = %g [mm]  \n\t pDx4 = %g [mm]  \n\t pAlp2 = %g [mm] " %(self.height,self.gamma,0,self.thk,0.001,0.001,0,self.thk,self.base,self.base,0)


class Cone(Object):
	def __init__(self,name,z,rmin1,rmin2,rmax1,rmax2,sphi,dphi):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.z = z
		self.rmin1 = rmin1
		self.rmin2 = rmin2
		self.rmax1 = rmax1
		self.rmax2 = rmax2
		self.sphi = sphi
		self.dphi = dphi
	def PrintConstruct(self):
		return "G4Cone: "+self.Name()+"\n\t z = %g [mm] \n\t rmin1 = %g [mm] \n\t rmin2 = %g [mm] \n\t rmax1 = %g [mm] \n\t rmax2 = %g [mm] \n\t sphi = %g [deg] \n\t dphi = %g [deg]" %(self.z,self.rmin1,self.rmin2,self.rmax1,self.rmax2,self.sphi,self.dphi)
	def Fill(self):
		return Cone(self.name+"_Fill",self.z,0,0,self.rmin1,self.rmin2,self.sphi,self.dphi)

class EllipticalTube(Object):
	def __init__(self,name,x,y,z):
		try:
			self.experiment.append(self)
		except(AttributeError):
			pass
		self.name = name
		self.x = x
		self.y = y
		self.z = z
	def PrintConstruct(self):
		return "G4Eltube: "+self.Name()+"\n\t x = %g [mm] \n\t y = %g [mm] \n\t z = %g [mm]" %(self.x,self.y,self.z)


class UnionChain:
	def __init__(self,objs):
		self.objects = objs
		# self.positions = infoSet[1]
		# self.rotations = infoSet[2]
		# return self.MakeUnionSolids()
	def MakeUnionSolids(self):
		names = [self.objects[0].Name()]
		unionObjects = [self.objects[0]]
		for i in range(len(self.objects)-1):
			names.append(names[-1]+"_PLUS_"+self.objects[i+1].Name())
			unionObjects.append(UnionSolid(names[-1],unionObjects[-1],self.objects[i+1],self.objects[i+1].GetPosition(),minus(self.objects[i+1].GetRotation())))
			if i < (len(self.objects)-2):
				unionObjects[-1].SetVirtual()
			if i == (len(self.objects)-2):
				return unionObjects[-1]
