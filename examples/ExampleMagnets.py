from __future__ import division
from math import *
from pyGeoGDML import *


analyzeAngle = 25.0
analyzeTheta = degToRad(analyzeAngle)
lumiAngle = 25

lumiTheta = degToRad(lumiAngle)

nominalAngle = 35
nominalTheta = degToRad(nominalAngle)

Al = "G4_Al"
Vac = "G4_Galactic"

print "Generating Example Geometry"

Example = Detector("ExampleMagnets")
SETEXPERIMENT(Example)


if(True): #Magnets!

	#################################################################
	# Magnet yoke
	#################################################################


	Yoke1 = Tube("Yoke1",  0.  ,785./2.,170.,180.,90.)
	Yoke1.SetVirtual()

	Yoke2 = Tube("Yoke2",335./2.,787./2.,110.,179.,92.)
	Yoke2.SetVirtual()

	YokeBox = Box("YokeBox", 130.815, 130.815, 171.)
	YokeBox.SetVirtual()

	YokeMinus1 =SubtractionSolid("YokeMinus1",Yoke1,Yoke2)
	YokeMinus1.SetVirtual()

	YokeMinus2 =SubtractionSolid("YokeMinus2",YokeMinus1,YokeBox,[0,0,0],[0,0,45])
	YokeMinus2.SetVirtual()#SetMaterial(Al)#.SetVirtual()#SetMaterial(Al)
	#YokeMinus2.SetPosition([-100,0,0])



	#**************************************************************************
	# Left Coil ***************************************************************
	#**************************************************************************


	PoleL = Tube("PoleL",435./2.,685./2.,39.238,180.,90.)
	PoleL.SetVirtual()#.SetMaterial(Al)
	#PoleL.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus1 = Box("PoleLBoxMinus1",131.758,13.415,43.415)
	PoleLBoxMinus1.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleLBoxMinus1.SetPosition([-280,0,-(39.238+30.000)/2])
	#PoleLBoxMinus1.SetRotation([-15,0,0])

	PoleLMinus1 = SubtractionSolid("PoleLMinus1",PoleL,PoleLBoxMinus1,[-280,13.415/2-3.472/2,-39.238/2+14.237+12.958/2.],[15,0,0])
	PoleLMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus1.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus2 = Box("PoleLBoxMinus1",138.632,21.4,41.4)
	PoleLBoxMinus2.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleLBoxMinus2.SetPosition([-280,-3.472,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleLBoxMinus2.SetRotation([-53.127,0,0])

	PoleLMinus2 = SubtractionSolid("PoleLMinus2",PoleLMinus1,PoleLBoxMinus2,[-280,-3.472,-39.238/2+14.237+12.958+12.841/2],[53.127,0,0])
	PoleLMinus2.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus2.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus3 = Box("PoleLBoxMinus3",13.415,131.758,43.415)
	PoleLBoxMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleLBoxMinus3.SetPosition([13.415/2-3.472/2,-280,-39.238+14.237+12.958/2.-(30.000)/2])
	#PoleLBoxMinus3.SetRotation([0,15,0])

	PoleLMinus3 = SubtractionSolid("PoleLMinus3",PoleLMinus2,PoleLBoxMinus3,[13.415/2.-3.472/2.,-280.,-39.238/2.+14.237+12.958/2.],[0,-15,0])
	PoleLMinus3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus3.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus4 = Box("PoleLBoxMinus4",21.4,138.632,41.4)
	PoleLBoxMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleLBoxMinus4.SetPosition([-3.472,-280,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleLBoxMinus4.SetRotation([0,53.127,0])

	PoleLMinus4 = SubtractionSolid("PoleLMinus4",PoleLMinus3,PoleLBoxMinus4,[-3.472,-280,-39.238/2+14.237+12.958+12.841/2+5],[0,-53.127,0])
	PoleLMinus4.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus4.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLCutTube1 = Tube("PoleLCutTube1",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleLCutTube1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube1.SetPosition([-435./2.-16.,-16.,-(39.238+30.000)/2])
	PoleLMinus5 = SubtractionSolid("PoleLMinus5",PoleLMinus4,PoleLCutTube1,[-435./2.-15.5,-16.01,0])
	PoleLMinus5.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus5.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2


	PoleLCutTube2 = Tube("PoleLCutTube2",32./2.,100./2.,39.438,90.,90.) #39.238
	PoleLCutTube2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube2.SetPosition([-685./2.+16.,-16.,-(39.238+30.000)/2])
	PoleLMinus6 = SubtractionSolid("PoleLMinus6",PoleLMinus5,PoleLCutTube2,[-685./2.+16.5,-16.01,0])
	PoleLMinus6.SetVirtual()#SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleLMinus6.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube3 = Tube("PoleLCutTube3",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleLCutTube3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube3.SetPosition([-16.,-435./2.-16.,-(39.238+30.000)/2])
	PoleLMinus7 = SubtractionSolid("PoleLMinus7",PoleLMinus6,PoleLCutTube3,[-16.01,-435./2.-15.5,0])
	PoleLMinus7.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus7.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube4 = Tube("PoleLCutTube4",32./2.,100./2.,39.438,270.,90.) #39.238
	PoleLCutTube4.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube4.SetPosition([-16.,-685./2.+16.,-(39.238+30.000)/2])
	PoleLMinus8 = SubtractionSolid("PoleLMinus8",PoleLMinus7,PoleLCutTube4,[-16.01,-685./2.+16.5,0])
	PoleLMinus8.SetVirtual()#.SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleLMinus8.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube5 = Tube("PoleLCutTube5",447.7/2.,672.3/2.,1.273,180.,90.) #39.238
	PoleLCutTube5.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube5.SetPosition([0,0,-(1.273+30.000)/2])
	PoleLMinus9 = SubtractionSolid("PoleLMinus9",PoleLMinus8,PoleLCutTube5,[0,0,(39.238-1.273)/2])
	PoleLMinus9.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus9.SetPosition([0,0,-(39.238+30.000)/2])

	#**************************************************************************
	#**************************************************************************




	#**************************************************************************
	# Right Coil ***************************************************************
	#**************************************************************************

	PoleR = Tube("PoleR",435./2.,685./2.,39.238,180.,90.)
	PoleR.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleR.SetPosition([0,0,(39.238+30.000)/2])

	PoleRBoxMinus1 = Box("PoleRBoxMinus1",131.758,13.415,43.415)
	PoleRBoxMinus1.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleRBoxMinus1.SetPosition([-280,0,-(39.238+30.000)/2])
	#PoleRBoxMinus1.SetRotation([-15,0,0])

	PoleRMinus1 = SubtractionSolid("PoleRMinus1",PoleR,PoleRBoxMinus1,[-280,13.415/2-3.472/2,39.238/2-14.237-12.958/2.],[-15,0,0])
	PoleRMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus1.SetPosition([0,0,-(39.238+30.000)/2])

	PoleRBoxMinus2 = Box("PoleRBoxMinus1",138.632,21.4,41.4)
	PoleRBoxMinus2.SetVirtual()#.SetVirtual()

	PoleRMinus2 = SubtractionSolid("PoleRMinus2",PoleRMinus1,PoleRBoxMinus2,[-280,-3.472,39.238/2-14.237-12.958-12.841/2],[-53.127,0,0])
	PoleRMinus2.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus2.SetPosition([0,0,(39.238+30.000)/2])

	PoleRBoxMinus3 = Box("PoleRBoxMinus3",13.415,131.758,43.415)
	PoleRBoxMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleRBoxMinus3.SetPosition([13.415/2-3.472/2,-280,-39.238+14.237+12.958/2.-(30.000)/2])
	#PoleRBoxMinus3.SetRotation([0,15,0])

	PoleRMinus3 = SubtractionSolid("PoleRMinus3",PoleRMinus2,PoleRBoxMinus3,[13.415/2.-3.472/2.,-280.,39.238/2.-14.237-12.958/2.],[0,15,0])
	PoleRMinus3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus3.SetPosition([0,0,-(39.238+30.000)/2])

	PoleRBoxMinus4 = Box("PoleRBoxMinus4",21.4,138.632,41.4)
	PoleRBoxMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleRBoxMinus4.SetPosition([-3.472,-280,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleRBoxMinus4.SetRotation([0,53.127,0])

	PoleRMinus4 = SubtractionSolid("PoleRMinus4",PoleRMinus3,PoleRBoxMinus4,[-3.472,-280,39.238/2-14.237-12.958-12.841/2-5],[0,53.127,0])
	PoleRMinus4.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus4.SetPosition([0,0,(39.238+30.000)/2])

	PoleRCutTube1 = Tube("PoleRCutTube1",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleRCutTube1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube1.SetPosition([-435./2.-16.,-16.,(39.238+30.000)/2])
	PoleRMinus5 = SubtractionSolid("PoleRMinus5",PoleRMinus4,PoleRCutTube1,[-435./2.-15.5,-16.01,0])
	PoleRMinus5.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus5.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube2 = Tube("PoleRCutTube2",32./2.,100./2.,39.438,90.,90.) #39.238
	PoleRCutTube2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube2.SetPosition([-685./2.+16.,-16.,(39.238+30.000)/2])
	PoleRMinus6 = SubtractionSolid("PoleRMinus6",PoleRMinus5,PoleRCutTube2,[-685./2.+16.5,-16.01,0])
	PoleRMinus6.SetVirtual()#SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleRMinus6.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube3 = Tube("PoleRCutTube3",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleRCutTube3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube3.SetPosition([-16.,-435./2.-16.,(39.238+30.000)/2])
	PoleRMinus7 = SubtractionSolid("PoleRMinus7",PoleRMinus6,PoleRCutTube3,[-16.01,-435./2.-15.5,0])
	PoleRMinus7.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus7.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube4 = Tube("PoleRCutTube4",32./2.,100./2.,39.438,270.,90.) #39.238
	PoleRCutTube4.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube4.SetPosition([-16.,-685./2.+16.,-(39.238+30.000)/2])
	PoleRMinus8 = SubtractionSolid("PoleRMinus8",PoleRMinus7,PoleRCutTube4,[-16.01,-685./2.+16.5,0])
	PoleRMinus8.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus8.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube5 = Tube("PoleRCutTube5",447.7/2.,672.3/2.,1.273,180.,90.) #39.238
	PoleRCutTube5.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube5.SetPosition([0,0,-(1.273+30.000)/2])
	PoleRMinus9 = SubtractionSolid("PoleRMinus9",PoleRMinus8,PoleRCutTube5,[0,0,-(39.238-1.273)/2])
	PoleRMinus9.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus9.SetPosition([0,0,(39.238+30.000)/2])
	#**************************************************************************
	#**************************************************************************





	#**************************************************************************
	# Left Coil ***************************************************************
	#**************************************************************************
	Coil1L = Tube("Coil1L",339./2.,431./2.,36.,180.,90.)
	#Coil1L.SetPosition([0,0,-36.])
	Coil1L.SetVirtual()#.SetMaterial("G4_W")
	Coil2L = Tube("Coil2L",689./2.,780.999/2.,36.,180.,90.)
	#Coil2L.SetPosition([0,0,-36.])
	Coil2L.SetVirtual()#.SetMaterial("G4_W")

	InY = -13.
	Coil3L = Tube("Coil3L",36./2.,128./2.,36.,0.,90.)
	#Coil3L.SetPosition([-232.5,InY,-36.])
	Coil3L.SetVirtual()#.SetMaterial("G4_W")
	Coil4L = Tube("Coil4L",36./2.,128./2.,36.,90.,80.)
	#Coil4L.SetPosition([-327.5,InY,-36.])
	Coil4L.SetVirtual()#.SetMaterial("G4_W")
	Coil5L = Box("Coil5L",96.103,46.,36.) #,93.103,46.,36.)
	#Coil5L.SetPosition([-280,5+46./2.,-36.])
	Coil5L.SetVirtual()#.SetMaterial("G4_W")

	InX = -13.
	Coil6L = Tube("Coil6L",36./2.,128./2.,36.,0.,90.)
	#Coil6L.SetPosition([InX,-232.5,-36.])
	Coil6L.SetVirtual()#.SetMaterial("G4_W")
	Coil7L = Tube("Coil7L",36./2.,128./2.,36.,270.,90.)
	#Coil7L.SetPosition([InX,-327.,-36.])
	Coil7L.SetVirtual()#.SetMaterial("G4_W")
	Coil8L = Box("Coil8L",46.,96.103,36.) #46.,93.103,36.)
	#Coil8L.SetPosition([5+46./2.,-280,-36.])
	Coil8L.SetVirtual()#.SetMaterial("G4_W")


	CoilLSum1 =UnionSolid("CoilLSum1",Coil1L,Coil2L,[0,0,0.])
	CoilLSum1.SetVirtual()#SetMaterial("G4_W")
	CoilLSum2 =UnionSolid("CoilLSum2",CoilLSum1,Coil3L,[-232.5,InY,0.])
	CoilLSum2.SetVirtual()#SetMaterial("G4_W")
	CoilLSum3 =UnionSolid("CoilLSum3",CoilLSum2,Coil4L,[-327.5,InY,0.])
	CoilLSum3.SetVirtual()#SetMaterial("G4_W")
	CoilLSum4 =UnionSolid("CoilLSum4",CoilLSum3,Coil5L,[-280,5+46./2.,0.])
	CoilLSum4.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum5 =UnionSolid("CoilLSum5",CoilLSum4,Coil6L,[InX,-232.5,0.])
	CoilLSum5.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum6 =UnionSolid("CoilLSum6",CoilLSum5,Coil7L,[InX,-327.,0.])
	CoilLSum6.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum7 =UnionSolid("CoilLSum7",CoilLSum6,Coil8L,[5+46./2.,-280,0.])
	CoilLSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	#**************************************************************************
	# Right Coil **************************************************************
	#**************************************************************************
	Coil1R = Tube("Coil1R",339./2.,431./2.,36.,180.,90.)
	#Coil1R.SetPosition([0,0,36.])
	Coil1R.SetVirtual()#.SetMaterial("G4_W")
	Coil2R = Tube("Coil2R",689./2.,780.999/2.,36.,180.,90.)
	#Coil2R.SetPosition([0,0,36.])
	Coil2R.SetVirtual()#.SetMaterial("G4_W")

	InY = -13.
	Coil3R = Tube("Coil3R",36./2.,128./2.,36.,0.,90.)
	#Coil3R.SetPosition([-232.5,InY,36.])
	Coil3R.SetVirtual()#.SetMaterial("G4_W")
	Coil4R = Tube("Coil4R",36./2.,128./2.,36.,90.,80.)
	#Coil4R.SetPosition([-327.5,InY,36.])
	Coil4R.SetVirtual()#.SetMaterial("G4_W")
	Coil5R = Box("Coil5R",96.103,46.,36.) #,93.103,46.,36.)
	#Coil5R.SetPosition([-280,5+46./2.,36.])
	Coil5R.SetVirtual()#.SetMaterial("G4_W")

	InX = -13.
	Coil6R = Tube("Coil6R",36./2.,128./2.,36.,0.,90.)
	#Coil6R.SetPosition([InX,-232.5,36.])
	Coil6R.SetVirtual()#.SetMaterial("G4_W")
	Coil7R = Tube("Coil7R",36./2.,128./2.,36.,270.,90.)
	#Coil7R.SetPosition([InX,-327.,36.])
	Coil7R.SetVirtual()#.SetMaterial("G4_W")
	Coil8R = Box("Coil8R",46.,96.103,36.) #46.,93.103,36.)
	#Coil8R.SetPosition([5+46./2.,-280,36.])
	Coil8R.SetVirtual()#.SetMaterial("G4_W")

	CoilRSum1 =UnionSolid("CoilRSum1",Coil1R,Coil2R,[0,0,0.])
	CoilRSum1.SetVirtual()#SetMaterial("G4_W")
	CoilRSum2 =UnionSolid("CoilRSum2",CoilRSum1,Coil3R,[-232.5,InY,0.])
	CoilRSum2.SetVirtual()#SetMaterial("G4_W")
	CoilRSum3 =UnionSolid("CoilRSum3",CoilRSum2,Coil4R,[-327.5,InY,0.])
	CoilRSum3.SetVirtual()#SetMaterial("G4_W")
	CoilRSum4 =UnionSolid("CoilRSum4",CoilRSum3,Coil5R,[-280,5+46./2.,0.])
	CoilRSum4.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum5 =UnionSolid("CoilRSum5",CoilRSum4,Coil6R,[InX,-232.5,0.])
	CoilRSum5.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum6 =UnionSolid("CoilRSum6",CoilRSum5,Coil7R,[InX,-327.,0.])
	CoilRSum6.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum7 =UnionSolid("CoilRSum7",CoilRSum6,Coil8R,[5+46./2.,-280,0.])
	CoilRSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	# globalX = -400.
	# globalY =  300.
	# globalZ =  200.

	globalCoilPos = PolarPosition(sqrSum([262.112000,0,183.533005]),analyzeAngle,279.688995)
	[globalX,globalY,globalZ] = globalCoilPos

	PolesFinal = UnionSolid("PolesFinal", PoleLMinus9, PoleRMinus9, [0,0,39.238+30.000])
	PolesFinal.SetVirtual()#.SetMaterial(Al)

	YokePlusPoles = UnionSolid("YokePlusPoles", YokeMinus2, PolesFinal, [0,0,-(39.238+30.000)/2])
	YokePlusPoles.SetMaterial(Al)#.SetVirtual()
	YokePlusPoles.SetPosition([globalX+0.,globalY+0.,globalZ+0.])
	YokePlusPoles.SetRotation([0,-90+analyzeAngle,0])

	Coils = UnionSolid("Coils",CoilLSum7,CoilRSum7,[0,0, 2*36.000])
	Coils.SetMaterial("G4_W")

	offsetDist = 36.0
	offX = offsetDist*cos(analyzeTheta)
	offZ = offsetDist*sin(analyzeTheta)
	Coils.SetPosition([globalX-offX,globalY,globalZ-offZ])
	Coils.SetRotation([0,-90+analyzeAngle,0])




	#################################################################
	# Magnet yoke LUMI spectrometer
	#################################################################

	LUMI_Yoke1 = Tube("LUMI_Yoke1",  0.  ,940./2.,350.,180.,90.)
	LUMI_Yoke1.SetVirtual()
	LUMI_Yoke2 = Tube("LUMI_Yoke2",302./2.,460./2.,236.,179.,92.)
	LUMI_Yoke2.SetVirtual()
	LUMI_Yoke3 = Tube("LUMI_Yoke3",660./2.,818./2.,236.,179.,92.)
	LUMI_Yoke3.SetVirtual()
	LUMI_YokeBox = Box("LUMI_YokeBox",  116.1, 116, 352.)
	LUMI_YokeBox.SetVirtual()

	LUMI_YokeBox1 = Box("LUMI_YokeBox1", 300, 20.785, 20.785)
	LUMI_YokeBox1.SetVirtual()#SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox1.SetPosition([0,0,-20.785])
	LUMI_YokeBox2 = Box("LUMI_YokeBox2", 300, 40, 40)
	LUMI_YokeBox2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	LUMI_YokeBoxMinus1 =SubtractionSolid("LUMI_YokeBoxMinus1",LUMI_YokeBox1,LUMI_YokeBox2, [0,-20*sqrt(2)/2,20*sqrt(2)/2], [45,0,0] )
	LUMI_YokeBoxMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBoxMinus1.SetPosition([-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2]) #-116/2*sqrt(2)
	#LUMI_YokeBoxMinus1.SetRotation([0,0,45])

	LUMI_YokeBox3 = Box("LUMI_YokeBox3", 300, 20.785, 20.785)
	LUMI_YokeBox3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox3.SetPosition([0,0,20.785])
	LUMI_YokeBox4 = Box("LUMI_YokeBox4", 300.1, 40, 40)
	LUMI_YokeBox4.SetVirtual()
	LUMI_YokeBoxMinus2 =SubtractionSolid("LUMI_YokeBoxMinus2",LUMI_YokeBox3,LUMI_YokeBox4, [0,-20*sqrt(2)/2,-20*sqrt(2)/2], [45,0,0] )
	LUMI_YokeBoxMinus2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBoxMinus1.SetPosition([-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2]) #-116/2*sqrt(2)
	#LUMI_YokeBoxMinus1.SetRotation([0,0,45])

	LUMI_YokeBox5 = Box("LUMI_YokeBox5", 28.087, 48.087, 350.1)
	LUMI_YokeBox5.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox5.SetPosition([-940./2.,0,0])
	#LUMI_YokeBox5.SetRotation([0,0,45])

	LUMI_YokeBox6 = Box("LUMI_YokeBox6", 28.087, 48.087, 350.1)
	LUMI_YokeBox6.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox6.SetPosition([-940./2.,0,0])
	#LUMI_YokeBox6.SetRotation([0,0,45])

	                   # name        z           rmin1  rmin2    rmax1  rmax2  sphi  dphi
	LUMI_Cone1 = Cone("LUMI_Cone1",  470.-400,  400.0  ,  470.  , 500. , 600. , 269. , 92.)
	LUMI_Cone1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone1.SetPosition([0,0,70./2.+175.-18.*sqrt(2)/2])
	#LUMI_Cone1.SetRotation([0,180,0])
	LUMI_Cone2 = Cone("LUMI_Cone2",  470.-400,  400.0  ,  470.  , 500. , 600. ,179. , 92.)
	LUMI_Cone2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone2.SetPosition([0,0,500+70./2.+175.-18.*sqrt(2)/2])
	#LUMI_Cone2.SetRotation([0,0,0])

	LUMI_Yoke4 = Tube("LUMI_Yoke4",400./2.,800./2.,20.,179.,92.)
	LUMI_Yoke4.SetVirtual()

	                   # name        z           rmin1  rmin2      rmax1  rmax2  sphi  dphi
	LUMI_Cone3 = Cone("LUMI_Cone3",  342.5-312.5,  312.5  ,  342.5  , 350. , 370. , 269. , 92.)
	LUMI_Cone3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone3.SetPosition([0,0,-20/2.-7.538*sqrt(2)/2])
	#LUMI_Cone3.SetRotation([0,180,0])
	LUMI_Cone4 = Cone("LUMI_Cone4",  342.5-312.5,  312.5  ,  342.5 , 350. , 370. ,179. , 92.)
	LUMI_Cone4.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone4.SetPosition([0.,0,+20/2.+7.538*sqrt(2)/2])
	#LUMI_Cone4.SetRotation([0,0,0])

	                   # name        z           rmin1  rmin2      rmax1  rmax2  sphi  dphi
	LUMI_Cone5 = Cone("LUMI_Cone5",  247.5-217.5, 190. , 220.,  217.5  ,  247.5  , 179. , 92.)
	LUMI_Cone5.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone5.SetPosition([0,0,-20/2.-7.538*sqrt(2)/2])
	#LUMI_Cone5.SetRotation([0,180,0])
	LUMI_Cone6 = Cone("LUMI_Cone6",   247.5-217.5, 190. , 220.,  217.5  ,  247.5 , 269. , 92.)
	LUMI_Cone6.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone6.SetPosition([0.,0,+20/2.+7.538*sqrt(2)/2])
	#LUMI_Cone6.SetRotation([0,180,0])

	LUMI_YokeBoxL1 = Box("LUMI_YokeBoxL1", 102.1, 28.868, 28.868)    # 30 deg
	LUMI_YokeBoxL1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL1.SetPosition([-280.,5.3,-10.-5.2])                # 30 deg
	#LUMI_YokeBoxL1.SetRotation([60,0,0])                            # 30 deg
	LUMI_YokeBoxL2 = Box("LUMI_YokeBoxL2", 102.1, 28.868, 28.868)    # 30 deg
	LUMI_YokeBoxL2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL2.SetPosition([-280.,5.3,10.+5.2])                 # 30 deg
	#LUMI_YokeBoxL2.SetRotation([-60,0,0])                           # 30 deg
	LUMI_YokeBoxL3 = Box("LUMI_YokeBoxL3", 28.868, 102.1, 28.868)    # 30 deg
	LUMI_YokeBoxL3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL3.SetPosition([5.3,-280.,-10.-5.2])                # 30 deg
	#LUMI_YokeBoxL3.SetRotation([0,30,0])                            # 30 deg
	LUMI_YokeBoxL4 = Box("LUMI_YokeBoxL4", 28.868, 102.1, 28.868)    # 30 deg
	LUMI_YokeBoxL4.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL4.SetPosition([5.3,-280.,10.+5.2])                 # 30 deg
	#LUMI_YokeBoxL4.SetRotation([0,-30,0])                           # 30 deg

	LUMI_YokeBoxL1a = Box("LUMI_YokeBoxL1a", 102.1, 28.868, 28.868)     # 60 deg
	LUMI_YokeBoxL1a.SetVirtual()#.SetMaterial(Al)#                      # 60 deg
	#LUMI_YokeBoxL1a.SetPosition([-280.,-5.25,-4.7])                    # 60 deg
	#LUMI_YokeBoxL1a.SetRotation([30,0,0])                              # 60 deg
	LUMI_YokeBoxL2a = Box("LUMI_YokeBoxL2a", 102.1,  28.868, 28.868)    # 60 deg
	LUMI_YokeBoxL2a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL2a.SetPosition([-280.,-5.25,4.7])                     # 60 deg
	#LUMI_YokeBoxL2a.SetRotation([-30,0,0])                             # 60 deg
	LUMI_YokeBoxL3a = Box("LUMI_YokeBoxL3a", 28.868, 102.1, 28.868)     # 60 deg
	LUMI_YokeBoxL3a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL3a.SetPosition([-5.25,-280.,-4.7])                    # 60 deg
	#LUMI_YokeBoxL3a.SetRotation([0,60,0])                              # 60 deg
	LUMI_YokeBoxL4a = Box("LUMI_YokeBoxL4a", 28.868, 102.1, 28.868)     # 60 deg
	LUMI_YokeBoxL4a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL4a.SetPosition([-5.25,-280.,4.7])                     # 60 deg
	#LUMI_YokeBoxL4a.SetRotation([0,-60,0])                             # 60 deg


	LUMI_YokeMinus1 =SubtractionSolid("LUMI_YokeMinus1",LUMI_Yoke1,LUMI_Yoke2)
	LUMI_YokeMinus1.SetVirtual()
	LUMI_YokeMinus2 =SubtractionSolid("LUMI_YokeMinus2",LUMI_YokeMinus1,LUMI_Yoke3)
	LUMI_YokeMinus2.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus3 =SubtractionSolid("LUMI_YokeMinus3",LUMI_YokeMinus2,LUMI_YokeBox,[0,0,0],[0,0,45])
	LUMI_YokeMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus4 =SubtractionSolid("LUMI_YokeMinus4",LUMI_YokeMinus3,LUMI_YokeBoxMinus1,[-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2],[0,0,-45])
	LUMI_YokeMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus5 =SubtractionSolid("LUMI_YokeMinus5",LUMI_YokeMinus4,LUMI_YokeBoxMinus2,[-116/2*sqrt(2)-20.785*sqrt(2)/2,0,175-20.745/2],[0,0,-45])
	LUMI_YokeMinus5.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus6 =SubtractionSolid("LUMI_YokeMinus6",LUMI_YokeMinus5,LUMI_YokeBox5,[-940./2.+0.1,0,0],[0,0,-45])
	LUMI_YokeMinus6.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus7 =SubtractionSolid("LUMI_YokeMinus7",LUMI_YokeMinus6,LUMI_YokeBox6,[0,-940./2.+0.1,0],[0,0,-45])
	LUMI_YokeMinus7.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus8 =SubtractionSolid("LUMI_YokeMinus8",LUMI_YokeMinus7,LUMI_Cone1,[0,0,70./2.+175.-18.*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus8.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus9 =SubtractionSolid("LUMI_YokeMinus9",LUMI_YokeMinus8,LUMI_Cone2,[0,0,-70./2.-175.+18.*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus9.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus10 =SubtractionSolid("LUMI_YokeMinus10",LUMI_YokeMinus9,LUMI_Yoke4,[0,0,0],[0,0,0])
	LUMI_YokeMinus10.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()



	InitBox = Box("InitBox",0.001,0.001,0.001)
	InitBox.SetVirtual()
	InitBox.SetPosition(LUMI_YokeMinus10.GetPosition())

	LUMI_YokeMinus11 =UnionSolid("LUMI_YokeMinus11",InitBox,LUMI_Cone3,[0.,0,-20/2.-7.538*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus11.SetVirtual()#SetMaterial("G4_W")#.SetVirtual()

	LUMI_YokeMinus12 =UnionSolid("LUMI_YokeMinus12",LUMI_YokeMinus11,LUMI_Cone4,[0.,0,+20/2.+7.538*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus12.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus13 =UnionSolid("LUMI_YokeMinus13",LUMI_YokeMinus12,LUMI_Cone5,[0,0,-20/2.-7.538*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus13.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus14 =UnionSolid("LUMI_YokeMinus14",LUMI_YokeMinus13,LUMI_Cone6,[0-0.,0,+20/2.+7.538*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus14.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus15 =UnionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1,[-280.,5.3,-10.-5.2],[-60,0,0])      # 30 deg
	# LUMI_YokeMinus15 =UnionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1a,[-280.,-5.25,-4.7],[-30,0,0])        # 60 deg
	LUMI_YokeMinus15.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus16 =UnionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2,[-280.,5.3,10.+5.2],[60,0,0])        # 30 deg
	# LUMI_YokeMinus16 =UnionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2a,[-280.,-5.25,4.7],[30,0,0])          # 60 deg
	LUMI_YokeMinus16.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus17 =UnionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3,[5.3,-280.,-10.-5.2],[0,-30,0])       # 30 deg
	# LUMI_YokeMinus17 =UnionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3a,[-5.25,-280.,-4.7],[0,-60,0])       # 60 deg
	LUMI_YokeMinus17.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus18 =SubtractionSolid("LUMI_YokeMinus18",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg TEST
	# LUMI_YokeMinus18 =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	LUMI_YokeMinus18.SetVirtual()


	LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus10,LUMI_YokeMinus18)       # 30 deg TEST
	# LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	LUMI_YokeAndPoles.SetMaterial("G4_W")


#BACKUP COPY!!!!
# LUMI_YokeMinus11 =SubtractionSolid("LUMI_YokeMinus11",LUMI_YokeMinus10,LUMI_Cone3,[0.,0,-20/2.-7.538*sqrt(2)/2],[0,180,0])
# LUMI_YokeMinus11.SetVirtual()#SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus12 =SubtractionSolid("LUMI_YokeMinus12",LUMI_YokeMinus11,LUMI_Cone4,[0.,0,+20/2.+7.538*sqrt(2)/2],[0,0,0])
# LUMI_YokeMinus12.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus13 =SubtractionSolid("LUMI_YokeMinus13",LUMI_YokeMinus12,LUMI_Cone5,[0,0,-20/2.-7.538*sqrt(2)/2],[0,0,0])
# LUMI_YokeMinus13.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus14 =SubtractionSolid("LUMI_YokeMinus14",LUMI_YokeMinus13,LUMI_Cone6,[0-0.,0,+20/2.+7.538*sqrt(2)/2],[0,180,0])
# LUMI_YokeMinus14.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus15 =SubtractionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1,[-280.,5.3,-10.-5.2],[-60,0,0])      # 30 deg
# # LUMI_YokeMinus15 =SubtractionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1a,[-280.,-5.25,-4.7],[-30,0,0])        # 60 deg
# LUMI_YokeMinus15.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus16 =SubtractionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2,[-280.,5.3,10.+5.2],[60,0,0])        # 30 deg
# # LUMI_YokeMinus16 =SubtractionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2a,[-280.,-5.25,4.7],[30,0,0])          # 60 deg
# LUMI_YokeMinus16.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus17 =SubtractionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3,[5.3,-280.,-10.-5.2],[0,-30,0])       # 30 deg
# # LUMI_YokeMinus17 =SubtractionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3a,[-5.25,-280.,-4.7],[0,-60,0])       # 60 deg
# LUMI_YokeMinus17.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()





#These make it break for some reason!
	# # LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg
	# LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg TEST
	# # LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	# LUMI_YokeAndPoles.SetMaterial("G4_W")
	# # LUMI_YokeAndPoles.SetMaterial(Al)

	#Test_Box = Box("Test_Box", 40., 40., 93.566)
	#Test_Box.SetMaterial(Al)#.SetVirtual()#
	#Test_Box.SetPosition([-330.,20.,-175.+57. + 93.566/2.])


	#**************************************************************************
	# Left LUMI_Coil ***************************************************************
	#**************************************************************************
	LUMI_Coil1L = Tube("LUMI_Coil1L",320./2.,440./2.,105.,180.,90.)
	#LUMI_Coil1L.SetPosition([0,0,-36.])
	LUMI_Coil1L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil2L = Tube("LUMI_Coil2L",680./2.,800./2.,105.,180.,90.)
	#LUMI_Coil2L.SetPosition([0,0,-36.])
	LUMI_Coil2L.SetVirtual()#.SetMaterial("G4_W")

	InY = -5.
	LUMI_Coil3L = Tube("LUMI_Coil3L",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil3L.SetPosition([-232.5,InY,-36.])
	LUMI_Coil3L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil4L = Tube("LUMI_Coil4L",20./2.,140./2.,105.,90.,90.)
	#LUMI_Coil4L.SetPosition([-327.5,InY,-36.])
	LUMI_Coil4L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil5L = Box("LUMI_Coil5L",104.066,60.,105.) #,93.103,60.,105.)
	#LUMI_Coil5L.SetPosition([-280,5+60./2.,-36.])
	LUMI_Coil5L.SetVirtual()#.SetMaterial("G4_W")

	InX = -5.
	LUMI_Coil6L = Tube("LUMI_Coil6L",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil6L.SetPosition([InX,-232.5,-36.])
	LUMI_Coil6L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil7L = Tube("LUMI_Coil7L",20./2.,140./2.,105.,270.,90.)
	#LUMI_Coil7L.SetPosition([InX,-327.,-36.])
	LUMI_Coil7L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil8L = Box("LUMI_Coil8L",60.,104.066,105.) #60.,93.103,105.)
	#LUMI_Coil8L.SetPosition([5+60./2.,-280,-36.])
	LUMI_Coil8L.SetVirtual()#.SetMaterial("G4_W")


	LUMI_CoilLSum1 =UnionSolid("LUMI_CoilLSum1",LUMI_Coil1L,LUMI_Coil2L,[0,0,0.])
	LUMI_CoilLSum1.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum2 =UnionSolid("LUMI_CoilLSum2",LUMI_CoilLSum1,LUMI_Coil3L,[-229.0,InY,0.])
	LUMI_CoilLSum2.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum3 =UnionSolid("LUMI_CoilLSum3",LUMI_CoilLSum2,LUMI_Coil4L,[-330.0,InY,0.])
	LUMI_CoilLSum3.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum4 =UnionSolid("LUMI_CoilLSum4",LUMI_CoilLSum3,LUMI_Coil5L,[-280,5+60./2.,0.])
	LUMI_CoilLSum4.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum5 =UnionSolid("LUMI_CoilLSum5",LUMI_CoilLSum4,LUMI_Coil6L,[InX,-229.0,0.])
	LUMI_CoilLSum5.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum6 =UnionSolid("LUMI_CoilLSum6",LUMI_CoilLSum5,LUMI_Coil7L,[InX,-330.0,0.])
	LUMI_CoilLSum6.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum7 =UnionSolid("LUMI_CoilLSum7",LUMI_CoilLSum6,LUMI_Coil8L,[5+60./2.,-280,0.])
	LUMI_CoilLSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	#**************************************************************************
	# Right LUMI_Coil **************************************************************
	#**************************************************************************
	LUMI_Coil1R = Tube("LUMI_Coil1R",320./2.,440./2.,105.,180.,90.)
	#LUMI_Coil1R.SetPosition([0,0,36.])
	LUMI_Coil1R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil2R = Tube("LUMI_Coil2R",680./2.,800./2.,105.,180.,90.)
	#LUMI_Coil2R.SetPosition([0,0,36.])
	LUMI_Coil2R.SetVirtual()#.SetMaterial("G4_W")

	InY = -5.
	LUMI_Coil3R = Tube("LUMI_Coil3R",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil3R.SetPosition([-232.5,InY,36.])
	LUMI_Coil3R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil4R = Tube("LUMI_Coil4R",20./2.,140./2.,105.,90.,90.)
	#LUMI_Coil4R.SetPosition([-327.5,InY,36.])
	LUMI_Coil4R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil5R = Box("LUMI_Coil5R",104.066,60.,105.) #,93.103,60.,105.)
	#LUMI_Coil5R.SetPosition([-280,5+60./2.,36.])
	LUMI_Coil5R.SetVirtual()#.SetMaterial("G4_W")

	InX = -5.
	LUMI_Coil6R = Tube("LUMI_Coil6R",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil6R.SetPosition([InX,-232.5,36.])
	LUMI_Coil6R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil7R = Tube("LUMI_Coil7R",20./2.,140./2.,105.,270.,90.)
	#LUMI_Coil7R.SetPosition([InX,-327.,36.])
	LUMI_Coil7R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil8R = Box("LUMI_Coil8R",60.,104.066,105.) #60.,93.103,105.)
	#LUMI_Coil8R.SetPosition([5+60./2.,-280,36.])
	LUMI_Coil8R.SetVirtual()#.SetMaterial("G4_W")

	LUMI_CoilRSum1 =UnionSolid("LUMI_CoilRSum1",LUMI_Coil1R,LUMI_Coil2R,[0,0,0.])
	LUMI_CoilRSum1.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum2 =UnionSolid("LUMI_CoilRSum2",LUMI_CoilRSum1,LUMI_Coil3R,[-229.0,InY,0.])
	LUMI_CoilRSum2.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum3 =UnionSolid("LUMI_CoilRSum3",LUMI_CoilRSum2,LUMI_Coil4R,[-330.0,InY,0.])
	LUMI_CoilRSum3.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum4 =UnionSolid("LUMI_CoilRSum4",LUMI_CoilRSum3,LUMI_Coil5R,[-280,5+60./2.,0.])
	LUMI_CoilRSum4.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum5 =UnionSolid("LUMI_CoilRSum5",LUMI_CoilRSum4,LUMI_Coil6R,[InX,-229.0,0.])
	LUMI_CoilRSum5.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum6 =UnionSolid("LUMI_CoilRSum6",LUMI_CoilRSum5,LUMI_Coil7R,[InX,-330.0,0.])
	LUMI_CoilRSum6.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum7 =UnionSolid("LUMI_CoilRSum7",LUMI_CoilRSum6,LUMI_Coil8R,[5+60./2.,-280,0.])
	LUMI_CoilRSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************




	LUMI_Coils = UnionSolid("LUMI_Coils",LUMI_CoilLSum7,LUMI_CoilRSum7,[0,0,26+105])
	LUMI_Coils.SetMaterial(Al)#..SetVirtual()
	LUMI_OffsetDist = (26+105)/2
	LUMI_Angle = -90-lumiAngle
	LUMI_Theta = degToRad(LUMI_Angle)
	LUMI_Dist = sqrSum([484.012,225.698])#634.585 #distance to yoke plane in mm
	LUMI_Pos0 = PolarPosition(LUMI_Dist,-lumiAngle,280)

	LUMI_Coils.SetPosition(add(LUMI_Pos0,[LUMI_OffsetDist*sin(LUMI_Theta),0,-LUMI_OffsetDist*cos(LUMI_Theta)]))
	LUMI_Coils.SetRotation([0,LUMI_Angle,0])
	LUMI_YokeAndPoles.SetRotation([0,LUMI_Angle,0])
	LUMI_YokeAndPoles.SetPosition(LUMI_Pos0)
	print LUMI_YokeAndPoles.GetPosition()





print "Done!"
Example.SaveAndGenerate()
