from pyGeoGDML import *

# analyzeAngle = 25
lumiAngle = 25

lumiTheta = degToRad(lumiAngle)

nominalAngle = 35
nominalTheta = degToRad(nominalAngle)

Al = "G4_Al"
Vac = "G4_Galactic"
