#!/usr/bin/env python

import ROOT as r

r.gSystem.Load("libGeom")
r.TGeoManager.Import("./examples/Root_ExampleMagnets.gdml")
r.gGeoManager.GetTopVolume().Draw("ogl")

raw_input("Press Enter to close...")
